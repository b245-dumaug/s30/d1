// NOTE: [SECTION] MongoDB aggregation

/*
  -to generate and perform operations to create filtered results that helps us analyze the data
*/

/*
  using aggreate method:
  -the "$match" is usded to pass the documents that meet the specified
  condition
  /conditions to the next stage or aggreation process

  Syntax:
    {$match:{filed: value}}
*/

db.fruits.aggregate([{ $match: { onSale: true } }]);

/*
  -The group is used to group the elements together and field-value data from the group element.

  Syntax:
    {$group: _id:  "fieldSetGroup"}
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: '$supplier_id', totalFruilts: { $sum: '$stock' } } },
]);

//Mini activity
// First get all the fruits that are color Yellow then group them into there respective supplier and
// get their available stocks

db.fruits.aggregate([
  { $match: { color: 'Yellow' } },
  { $group: { _id: '$supplier_id', availableStocks: { $sum: '$stock' } } },
]);

//Field Projection with agregation
/*
  -$project can be used when aggregation data is include / exclude from the returned results.

  Syntax:
    {$project: {field : 1/0}}
*/

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: '$supplier_id', totalFruilts: { $sum: '$stock' } } },
  { $project: { _id: 0 } },
]);

// Sorting aggregated results

/*
      -$sort can be used to change the order tyo aggregated results
 */

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: '$supplier_id', totalFruilts: { $sum: '$stock' } } },
  { $project: { _id: 0 } },
  { $sort: { totalFruilts: 1 } },
]);

// The value in sort
// 1 - lowers to highest
// -1 - highest to lowest

//agreeagting results based on an array feilds

/*
  the $unwind deconstructs and array field from a collection / field with an array value to output a results
*/

db.fruits.aggregate([
  { $unwind: '$origin' },
  { $group: { _id: '$origin', fruits: { $sum: '$stocks' } } },
]);

// NOTE: [SECTIOn] Other aggregate stages
// $count all yellow fruis

db.fruits.aggregate([
  { $match: { color: 'Yellow' } },
  { $count: 'Yellow Fruits' },
]);

//$avg gets the average value of the stock

db.fruits.aggregate([
  { $match: { color: 'Yellow' } },
  { $group: { _id: '$color', avgYellow: { $avg: '$stock' } } },
]);

//$min

db.fruits.aggregate([
  { $match: { color: 'Yellow' } },
  { $group: { _id: '$color', lowestStock: { $min: '$stock' } } },
]);

//  $max
db.fruits.aggregate([
  { $match: { color: 'Yellow' } },
  { $group: { _id: '$color', highestStock: { $max: '$stock' } } },
]);
